package com.estudos.algamoney.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.estudos.algamoney.api.model.Lancamento;
import com.estudos.algamoney.api.repository.lancamento.LancamentoRepositoryQuery;

public interface LancamentoRepository extends JpaRepository<Lancamento, Long>, LancamentoRepositoryQuery{

}
