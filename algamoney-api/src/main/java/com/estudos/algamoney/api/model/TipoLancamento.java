package com.estudos.algamoney.api.model;

public enum TipoLancamento {

	RECEITA, DESPESA
}
